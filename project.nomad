job "project" {
 datacenters = ["dc1"]
 type = "service"

  group "project" {
    

    task "project" {
      
      driver = "docker"

      config {
        driver = "docker"
            config {
                image = "test-nomad:${CI_COMMIT_SHORT_SHA}"
                force_pull = true
                port_map {
                    http = 8080
                }
            }
            resources {
                network {
                    port "http" {}
                }
            }
      } # config
    } # task run
 } # group
} # job
